import { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { constructTree } from '@utils'
import type { TreeNode } from '@types'

const Graphviz = dynamic(() => import('graphviz-react'), { ssr: false })

const DotGraph = (props: { arrayInput: (number | null)[] }) => {
  const { arrayInput } = props
  const [dot, setDot] = useState('')

  const buildDigraph = (input: (number | null)[]) => {
    const dataTree = constructTree(input)

    const allNodes: (number | null)[] = []
    const allLinks: (number | null)[][] = []

    const findNodes = (root: TreeNode): number => {
      const i = allNodes.length
      allNodes.push(root.val)

      if (root.left) {
        const leftIndex = findNodes(root.left)
        allLinks.push([i, leftIndex])
      } else if (root.right) {
        const i2 = allNodes.length
        allNodes.push(null)
        allLinks.push([i, i2])
      }
      if (root.right) {
        const rightIndex = findNodes(root.right)
        allLinks.push([i, rightIndex])
      } else if (root.left) {
        const i2 = allNodes.length
        allNodes.push(null)
        allLinks.push([i, i2])
      }
      return i
    }
    if (dataTree) {
      findNodes(dataTree)
    }

    const data = {
      p: allNodes.length,
      nodes: allNodes,
      links: allLinks,
    }

    const g = `digraph {
		${data.nodes
      .map((x, i) =>
        x
          ? `${i} [label="${x.toLocaleString()}"]`
          : `${i} [label="null";style="dashed"]`
      )
      .join('\n')}
		${data.links.map(([i, j]) => `${i}->${j}`).join('\n')}
		}`

    setDot(g)
  }

  useEffect(() => {
    buildDigraph(arrayInput)
  }, [arrayInput])

  return (
    <div className="flex justify-center bg-blue-100">
      <Graphviz dot={dot} />
    </div>
  )
}

export default DotGraph
