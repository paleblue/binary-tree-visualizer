import type { TreeNode } from '@types'

export function constructTree(values: (number | null)[]): TreeNode | null {
  if (values === null || values.length === 0) {
    return null
  } else {
    const root = { val: values.shift() ?? null, left: null, right: null }
    const nodeQueue: (TreeNode | null)[] = [root]

    let timesLeft = 10
    let queueCount = 0
    while (nodeQueue.length > 0 && timesLeft > 0) {
      queueCount = nodeQueue.length

      for (let i = 0; i < queueCount; i++) {
        const leftValue = values.shift()
        const rightValue = values.shift()

        if (leftValue === null && rightValue === null) {
          continue
        }

        if (leftValue) {
          nodeQueue[i]!.left = { val: leftValue, left: null, right: null }
          if (values.length > 0) {
            nodeQueue.push(nodeQueue[i]!.left)
          }
        }

        if (rightValue) {
          nodeQueue[i]!.right = { val: rightValue, left: null, right: null }
          if (values.length > 0) {
            nodeQueue.push(nodeQueue[i]!.right)
          }
        }
      }

      nodeQueue.splice(0, queueCount)
      timesLeft--
    }

    return root
  }
}
