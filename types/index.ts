export type TreeNode = {
  val: number | null
  left: TreeNode | null
  right: TreeNode | null
}
