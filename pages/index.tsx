import type { NextPage } from 'next'
import { useState } from 'react'
import Head from 'next/head'
import DotGraph from '@components/DotGraph'

const Home: NextPage = () => {
  const [treeArray, setTreeArray] = useState('[1, 2, 3]')

  const [treeInput, setTreeInput] = useState<(number | null)[]>([1, 2, 3])

  const handleArrayUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTreeArray(event.currentTarget.value)
  }

  const handleDrawClick = () => {
    setTreeInput(JSON.parse(treeArray))
  }

  return (
    <div>
      <Head>
        <title>Binary Tree visualizer</title>
        <meta
          name="description"
          content="Generate binary tree visualizations using Layer Order input"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col justify-center w-full h-full bg-red-500 align-start">
        <div className="flex p-2 bg-blue-100 space-x-10">
          <input
            value={treeArray}
            onChange={handleArrayUpdate}
            type="text"
            className="flex-1 text-[16px] p-2"
          />

          <button
            className="p-2 text-white bg-blue-600"
            onClick={handleDrawClick}
          >
            draw
          </button>
        </div>

        <DotGraph arrayInput={treeInput} />
      </main>
    </div>
  )
}

export default Home
